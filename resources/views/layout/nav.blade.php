
<header id="header-top">
	<div class="container clearfix">
		<div class="row">
			<div class="col-md-6">
				<div class="phone-email"><i class="fa fa-phone"></i>+ 2 0106 5370701</div>
				<div class="phone-email phone-email-2"><i class="fa fa-envelope"></i>7oroof@7oroof.com</div>
			</div>
			<div class="col-md-6">
				<div class="social-ul">
					<ul>
						<li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li class="social-pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
						<li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
						<li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li class="social-dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
						<li class="social-youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
						<li class="social-rss"><a href="#"><i class="fa fa-rss"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div><!-- End container -->
</header><!-- End header -->
<header id="header">
	<div class="container clearfix">
		<div class="logo"><a href="index.html"><img alt="" src="/images/logo.png"></a><span>Business HTML Template</span></div>
		<div class="header-search">
			<div class="header-search-a"><i class="fa fa-search"></i></div>
			<div class="header-search-form">
				<form method="post">
					<input type="text" placeholder="Search Words Here">
				</form>
			</div>
		</div>
		<nav class="navigation">
			<ul>
				<li class="current_page_item"><a href="index.html">Home</a>
					<ul>
						<li><a href="index.html">Home 1</a></li>
						<li><a href="index-2.html">Home 2</a></li>
						<li><a href="index-3.html">Home 3</a></li>
						<li><a href="index-4.html">Home 4</a></li>
						<li><a href="index-5.html">Home 5</a></li>
						<li><a href="index-6.html">Home 6</a></li>
						<li><a href="index-7.html">Home 7</a></li>
						<li class="current_page_item"><a href="index-animate.html">Home Animate</a></li>
						<li><a href="index-one-page.html">Home One Page</a></li>
						<li><a href="index-boxed.html">Home Boxed 1</a></li>
						<li><a href="index-boxed-2.html">Home Boxed 2</a></li>
					</ul>
				</li>
				<li><a href="features.html">Features</a>
					<ul>
						<li><a href="index.html">Headers</a>
							<ul>
								<li><a href="index.html">Header 1</a></li>
								<li><a href="header-2.html">Header 2</a></li>
								<li><a href="header-3.html">Header 3</a></li>
								<li><a href="header-4.html">Header 4</a></li>
								<li><a href="header-5.html">Header 5</a></li>
								<li><a href="header-6.html">Header 6</a></li>
								<li><a href="fixed-nav.html">Fixed Navigation</a></li>
							</ul>
						</li>
						<li><a href="features.html">Features</a></li>
						<li><a href="about.html">About</a>
							<ul>
								<li><a href="about.html">About 1</a></li>
								<li><a href="about-2.html">About 2</a></li>
								<li><a href="about-3.html">About 3</a></li>
							</ul>
						</li>
						<li><a href="testimonials.html">Testimonials</a></li>
						<li><a href="faq.html">FAQ</a>
							<ul>
								<li><a href="faq.html">FAQ Right Sidebar</a></li>
								<li><a href="faq-left-sidebar.html">FAQ Left Sidebar</a></li>
								<li><a href="faq-centered.html">FAQ Centered</a></li>
							</ul>
						</li>
						<li><a href="team.html">Team</a>
							<ul>
								<li><a href="team.html">Team</a></li>
								<li><a href="team-right-sidebar.html">Team Right Sidebar</a></li>
								<li><a href="team-left-sidebar.html">Team Left Sidebar</a></li>
							</ul>
						</li>
						<li><a href="pricing-tables.html">Pricing Tables</a></li>
						<li><a href="typography.html">Typography</a></li>
						<li><a href="right-sidebar.html">Pages</a>
							<ul>
								<li><a href="right-sidebar.html">Right Sidebar</a></li>
								<li><a href="left-sidebar.html">Left Sidebar</a></li>
								<li><a href="full-width.html">Full Width</a></li>
								<li><a href="page-centered.html">Page Centered</a></li>
							</ul>
						</li>
						<li><a href="animation.html">Animation</a></li>
						<li><a href="404.html">404</a></li>
						<li><a href="coming-soon.html">Coming Soon</a></li>
					</ul>
				</li>
				<li><a href="services.html">Services</a>
					<ul>
						<li><a href="services.html">Services 1</a></li>
						<li><a href="services-2.html">Services 2</a></li>
						<li><a href="services-3.html">Services 3</a></li>
					</ul>
				</li>
				<li class="mega-menu"><a href="portfolio-description.html">Portfolio</a>
					<ul>
						<li class="col-md-4"><a href="portfolio-2-columns.html">Portfolio 2 Columns</a>
							<ul>
								<li><a href="portfolio-2-columns.html">Portfolio 1 Right Sidebar</a></li>
								<li><a href="portfolio-2-left-sidebar.html">Portfolio 1 Left Sidebar</a></li>
								<li><a href="portfolio-2-columns-full-width.html">Portfolio 1 Full Width</a></li>
								<li><a href="portfolio-gallery-2-columns.html">Portfolio 2 Right Sidebar</a></li>
								<li><a href="portfolio-gallery-left-sidebar-2-columns.html">Portfolio 2 Left Sidebar</a></li>
								<li><a href="portfolio-gallery-full-width-2-columns.html">Portfolio 2 Full Width</a></li>
								<li><a href="portfolio-2-columns-no-margin.html">Portfolio Right Sidebar no margin</a></li>
								<li><a href="portfolio-2-columns-no-margin-left-sidebar.html">Portfolio Left Sidebar no margin</a></li>
								<li><a href="portfolio-2-columns-no-margin-full-width.html">Portfolio Full Width no margin</a></li>
							</ul>
						</li>
						<li class="col-md-4"><a href="portfolio-3-columns.html">Portfolio 3 Columns</a>
							<ul>
								<li><a href="portfolio-3-columns.html">Portfolio 1 Right Sidebar</a></li>
								<li><a href="portfolio-3-left-sidebar.html">Portfolio 1 Left Sidebar</a></li>
								<li><a href="portfolio-3-columns-full-width.html">Portfolio 1 Full Width</a></li>
								<li><a href="portfolio-gallery-3-columns.html">Portfolio 2 Right Sidebar</a></li>
								<li><a href="portfolio-gallery-left-sidebar-3-columns.html">Portfolio 2 Left Sidebar</a></li>
								<li><a href="portfolio-gallery-full-width-3-columns.html">Portfolio 2 Full Width</a></li>
								<li><a href="portfolio-3-columns-no-margin.html">Portfolio Right Sidebar no margin</a></li>
								<li><a href="portfolio-3-columns-no-margin-left-sidebar.html">Portfolio Left Sidebar no margin</a></li>
								<li><a href="portfolio-3-columns-no-margin-full-width.html">Portfolio Full Width no margin</a></li>
							</ul>
						</li>
						<li class="col-md-4"><a href="portfolio-description.html">Portfolio 4 Columns</a>
							<ul>
								<li><a href="portfolio-description.html">Portfolio 1 Right Sidebar</a></li>
								<li><a href="portfolio-description-left-sidebar.html">Portfolio 1 Left Sidebar</a></li>
								<li><a href="portfolio-description-full-width.html">Portfolio 1 Full Width</a></li>
								<li><a href="portfolio-gallery.html">Portfolio 2 Right Sidebar</a></li>
								<li><a href="portfolio-gallery-left-sidebar.html">Portfolio 2 Left Sidebar</a></li>
								<li><a href="portfolio-gallery-full-width.html">Portfolio 2 Full Width</a></li>
								<li><a href="portfolio-4-columns-no-margin.html">Portfolio Right Sidebar no margin</a></li>
								<li><a href="portfolio-4-columns-no-margin-left-sidebar.html">Portfolio Left Sidebar no margin</a></li>
								<li><a href="portfolio-4-columns-no-margin-full-width.html">Portfolio Full Width no margin</a></li>
							</ul>
						</li>
						<li class="col-md-4"><a href="portfolio-description.html">Portfolio Carousel</a>
							<ul>
								<li><a href="carousel-portfolio-2-columns.html">Carousel 2 Columns</a></li>
								<li><a href="carousel-portfolio-2-columns-left-sidebar.html">Carousel 2 Columns Left Sidebar</a></li>
								<li><a href="carousel-portfolio-2-columns-full-width.html">Carousel 2 Columns Full Width</a></li>
								<li><a href="carousel-portfolio-3-columns.html">Carousel 3 Columns</a></li>
								<li><a href="carousel-portfolio-3-columns-left-sidebar.html">Carousel 3 Columns Left Sidebar</a></li>
								<li><a href="carousel-portfolio-3-columns-full-width.html">Carousel 3 Columns Full Width</a></li>
								<li><a href="carousel-portfolio-4-columns.html">Carousel 4 Columns</a></li>
								<li><a href="carousel-portfolio-4-columns-left-sidebar.html">Carousel 4 Columns Left Sidebar</a></li>
								<li><a href="carousel-portfolio-4-columns-full-width.html">Carousel 4 Columns Full Width</a></li>
							</ul>
						</li>
						<li class="col-md-4"><a href="single-portfolio.html">Single Portfolio</a>
							<ul>
								<li><a href="single-portfolio.html">Single Portfolio 1</a></li>
								<li><a href="single-portfolio-2.html">Single Portfolio 2</a></li>
								<li><a href="single-portfolio-3.html">Single Portfolio 3</a></li>
								<li><a href="single-portfolio-right-sidebar.html">Single Portfolio Right Sidebar</a></li>
								<li><a href="single-portfolio-left-sidebar.html">Single Portfolio Left Sidebar</a></li>
							</ul>
						</li>
						<li class="col-md-4"><a href="portfolio-3.html">Portfolio Full width</a>
							<ul>
								<li><a href="portfolio-3.html">Portfolio 4 Columns</a></li>
								<li><a href="portfolio-3-5-columns.html">Portfolio 5 Columns</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="blog.html">Blog</a>
					<ul>
						<li><a href="blog.html">Blog 1</a>
							<ul>
								<li><a href="blog.html">Blog 1 Right Sidebar</a></li>
								<li><a href="blog-1-left-sidebar.html">Blog 1 Left Sidebar</a></li>
								<li><a href="blog-1-full-width.html">Blog 1 Full Width</a></li>
							</ul>
						</li>
						<li><a href="blog-2.html">Blog 2</a>
							<ul>
								<li><a href="blog-2.html">Blog 2 Right Sidebar</a></li>
								<li><a href="blog-2-left-sidebar.html">Blog 2 Left Sidebar</a></li>
								<li><a href="blog-2-full-width.html">Blog 2 Full Width</a></li>
							</ul>
						</li>
						<li><a href="single-blog.html">Single Blog</a>
							<ul>
								<li><a href="single-blog.html">Single Blog Right Sidebar</a></li>
								<li><a href="single-blog-left-sidebar.html">Single Blog Left Sidebar</a></li>
								<li><a href="single-blog-full-width.html">Single Blog Full Width</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="shortcodes.html">Shortcodes</a></li>
				<li><a href="contact.html">Contact</a>
					<ul>
						<li><a href="contact.html">Contact 1</a></li>
						<li><a href="contact-2.html">Contact 2</a></li>
						<li><a href="contact-3.html">Contact 3</a></li>
						<li><a href="contact-4.html">Contact 4</a></li>
						<li><a href="contact-5.html">Contact 5</a></li>
					</ul>
				</li>
			</ul>
		</nav><!-- End navigation -->
	</div><!-- End container -->
</header><!-- End header -->

<div class="clearfix"></div>