<div class="slideshow">
	<div class="tp-banner-container">
		<div class="tp-banner">
			<ul>
				<li data-transition="random" data-slotamount="7" data-masterspeed="1500">
					<!-- MAIN IMAGE -->
					<img src="http://placehold.it/1600x532/171717/FFF/" alt="" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					
					<div class="slideshow-bg"
					data-y="310"
					data-x="center"
					data-start="0"></div>
					<!-- LAYERS -->
					
					<!-- LAYER NR. 1 -->
					<div class="slide-h2 tp-caption randomrotate skewtoleft tp-resizeme start white"
						data-y="160"
						data-x="center"
						data-hoffset="0"
						data-start="300"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 2"><h2>Why Ori ?</h2>
					</div>
					
					<!-- LAYER NR. 2 -->
					<div class="slide-h3 tp-caption customin white"
						data-y="230"
						data-x="center"
						data-hoffset="0"
						data-start="600"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 2"><h2>Awesome, Clean & Unique Template</h2>
					</div>
	
					<!-- LAYER NR. 3 -->
					<div class="slide-p tp-caption black randomrotate skewtoleft tp-resizeme start" 
						data-x="center" 
						data-hoffset="0" 
						data-y="310" 
						data-speed="500" 
						data-start="1300" 
						data-easing="Power3.easeInOut" 
						data-splitin="none" 
						data-splitout="none" 
						data-elementdelay="0.1" 
						data-endelementdelay="0.1" 
						data-endspeed="500" style="z-index: 6;white-space: pre-line;max-width: 1170px;text-align: center;"><p class="white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</div>
					
					<!-- LAYER NR. 4 -->
					<div class="slide-a tp-caption customin"
						data-x="center"
						data-y="390"
						data-hoffset="100"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="1800"
						data-easing="Power3.easeInOut"
						data-endspeed="500"
						style="z-index: 4"><a href="#" class="button-4">Ori Features</a>
					</div>
					
					<!-- LAYER NR. 5 -->
					<div class="slide-a slide-a-2 tp-caption customin"
						data-x="center"
						data-y="390"
						data-hoffset="-100"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="2100"
						data-easing="Power3.easeInOut"
						data-endspeed="500"
						style="z-index: 4"><a href="http://themeforest.net/user/begha" class="button-3">Purchase Ori</a>
					</div>
				</li>
				<li data-transition="random" data-slotamount="7" data-masterspeed="1000">
					<!-- MAIN IMAGE -->
					<img src="http://placehold.it/1600x532/171717/FFF/" alt="" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					
					<div class="slideshow-bg"
					data-y="310"
					data-x="center"
					data-start="0"></div>
					<!-- LAYERS -->
					
					<!-- LAYER NR. 1 -->
					<div class="tp-caption lfl start"
						data-x="600"
						data-y="100"
						data-speed="400"
						data-start="1300"
						data-easing="easeOut"
						data-endspeed="500"><img src="images/macbookpro.png" alt="macbookpro">
					</div>
					
					<!-- Layer NR. 2 -->
					<div class="tp-caption color large_bg sfr start"
						data-x="0"
						data-y="110"
						data-start="1800"
						data-speed="700"
						data-easing="easeOutBounce"
						style="z-index: 4">Awesome & Great Shortcodes
					</div>
					
					<!-- Layer NR. 3 -->
					<div class="tp-caption color large_bg sft start"
						data-x="0"
						data-y="160"
						data-start="2300"
						data-speed="700"
						data-easing="easeOutBounce"
						style="z-index: 4">Fully Responsive Layout
					</div>
					
					<!-- Layer NR. 4 -->
					<div class="tp-caption color large_bg sfl start"
						data-x="0"
						data-y="210"
						data-start="2800"
						data-speed="700"
						data-easing="easeOutBounce"
						style="z-index: 4">Advanced Features
					</div>
					
					<!-- Layer NR. 5 -->
					<div class="tp-caption color large_bg sft start"
						data-x="0"
						data-y="260"
						data-start="3300"
						data-speed="700"
						data-easing="easeOutBounce"
						style="z-index: 4">Multi-Purpose
					</div>
					
					<!-- Layer NR. 6 -->
					<div class="tp-caption color large_bg sfr start"
						data-x="0"
						data-y="310"
						data-start="3800"
						data-speed="700"
						data-easing="easeOutBounce"
						style="z-index: 4">Very Easy To Customize
					</div>
					
					<!-- Layer NR. 7 -->
					<div class="tp-caption color large_bg sft start"
						data-x="0"
						data-y="360"
						data-start="4300"
						data-speed="700"
						data-easing="easeOutBounce"
						style="z-index: 4">Google Fonts
					</div>
				</li>
				
				<li data-transition="random" data-slotamount="7" data-masterspeed="1000">
					<!-- MAIN IMAGE -->
					<img src="http://placehold.it/1600x532/171717/FFF/" alt="" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					
					<div class="slideshow-bg"
					data-y="310"
					data-x="center"
					data-start="0"></div>
					<!-- LAYERS -->
					
					<!-- LAYER NR. 1 -->
					<div class="tp-caption lfl start"
						data-x="300"
						data-y="150"
						data-speed="400"
						data-start="1300"
						data-easing="easeOut"
						data-endspeed="500"><img src="images/macbookpro.png" alt="macbookpro">
					</div>
					
					<!-- Layer NR. 2 -->
					<div class="slide-h2 tp-caption randomrotate skewtoleft tp-resizeme start white"
						data-y="60"
						data-x="center"
						data-hoffset="0"
						data-start="2200"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 2"><h2>Hello, We Are Ori</h2>
					</div>
					
					<!-- Layer NR. 3 -->
					<div class="tp-caption color large_bg sfr start"
						data-x="0"
						data-y="110"
						data-start="2900"
						data-speed="700"
						data-easing="easeOutBounce"
						style="z-index: 4">Awesome & Great Shortcodes
					</div>
					
					<!-- Layer NR. 4 -->
					<div class="tp-caption color large_bg sft start"
						data-x="0"
						data-y="160"
						data-start="3400"
						data-speed="700"
						data-easing="easeOutBounce"
						style="z-index: 4">Fully Responsive Layout
					</div>
					
					<!-- Layer NR. 5 -->
					<div class="tp-caption color large_bg sfl start"
						data-x="0"
						data-y="210"
						data-start="3900"
						data-speed="700"
						data-easing="easeOutBounce"
						style="z-index: 4">Advanced Features
					</div>
					
					<!-- Layer NR. 6 -->
					<div class="tp-caption color large_bg sft start"
						data-x="900"
						data-y="110"
						data-start="4400"
						data-speed="700"
						data-easing="easeOutBounce"
						style="z-index: 4">Ori Is Very Easy To Customize
					</div>
					
					<!-- Layer NR. 7 -->
					<div class="tp-caption color large_bg sfr start"
						data-x="930"
						data-y="160"
						data-start="4900"
						data-speed="700"
						data-easing="easeOutBounce"
						style="z-index: 4">Business & Multi-Purpose
					</div>
					
					<!-- Layer NR. 8 -->
					<div class="tp-caption color large_bg sft start"
						data-x="985"
						data-y="210"
						data-start="5400"
						data-speed="700"
						data-easing="easeOutBounce"
						style="z-index: 4">Google Fonts Used
					</div>
				</li>
				<!-- SLIDE  -->
				<li data-transition="random" data-slotamount="7" data-masterspeed="1000">
					<!-- MAIN IMAGE -->
					<img src="http://placehold.it/1600x532/e03e25/FFF/" alt="" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					
					<div class="slideshow-bg"
					data-y="310"
					data-x="center"
					data-start="0"></div>
					<!-- LAYERS -->
					
					<!-- LAYER NR. 1 -->
					<div class="slide-h2 tp-caption randomrotate skewtoleft tp-resizeme start white"
						data-y="130"
						data-x="center"
						data-hoffset="0"
						data-start="300"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 2"><h2>Why Ori ?</h2>
					</div>
					
					<!-- LAYER NR. 2 -->
					<div class="slide-h3 tp-caption customin white"
						data-y="200"
						data-x="center"
						data-hoffset="0"
						data-start="600"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-easing="Power3.easeInOut"
						data-endspeed="300"
						style="z-index: 2"><h2>Awesome, Clean & Unique Template</h2>
					</div>
	
					<!-- LAYER NR. 3 -->
					<div class="slide-p tp-caption black randomrotate skewtoleft tp-resizeme start" 
						data-x="center" 
						data-hoffset="0" 
						data-y="280" 
						data-speed="500" 
						data-start="1300" 
						data-easing="Power3.easeInOut" 
						data-splitin="none" 
						data-splitout="none" 
						data-elementdelay="0.1" 
						data-endelementdelay="0.1" 
						data-endspeed="500" style="z-index: 6;white-space: pre-line;max-width: 1170px;text-align: center;"><p class="white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</div>
					
					<!-- LAYER NR. 4 -->
					<div class="slide-a slide-a-2 tp-caption customin"
						data-x="center"
						data-y="360"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="1800"
						data-easing="Power3.easeInOut"
						data-endspeed="500"
						style="z-index: 4"><a href="http://themeforest.net/user/begha" class="button-6">Purchase Ori</a>
					</div>
				</li>
			</ul>
		</div>
	</div><!-- End tp-banner-container -->
</div><!-- End slideshow -->

<div class="sections">
	<div class="container">
		<div class="animation" data-animate="bounceInRight">
			<div class="sections-title">
				<div class="sections-title-h3"><h3>Awesome & Great Services</h3></div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.</p>
			</div><!-- End sections-title -->
		</div><!-- End animation -->
		<div class="row">
			<div class="animation" data-animate="bounceInLeft">
				<div class="bxslider-slide box-icon-slide">
					<ul>
						<li>
							<div class="col-md-3">
								<div class="box-icon">
									<div class="box-icon-i box-icon-i-2"><i class="fa fa-heart"></i></div>
									<div class="box-icon-content">
										<h5>Creative & Awesome</h5>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Integer lorem quam adipiscing. <a href="#">Read More</a></p>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="box-icon">
									<div class="box-icon-i box-icon-i-2"><i class="fa fa-desktop"></i></div>
									<div class="box-icon-content">
										<h5>Responsive Layout</h5>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Integer lorem quam adipiscing. <a href="#">Read More</a></p>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="box-icon">
									<div class="box-icon-i box-icon-i-2"><i class="fa fa-briefcase"></i></div>
									<div class="box-icon-content">
										<h5>Advanced Features</h5>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Integer lorem quam adipiscing. <a href="#">Read More</a></p>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="box-icon">
									<div class="box-icon-i box-icon-i-2"><i class="fa fa-gears"></i></div>
									<div class="box-icon-content">
										<h5>Multi-Purpose</h5>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Integer lorem quam adipiscing. <a href="#">Read More</a></p>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="col-md-3">
								<div class="box-icon">
									<div class="box-icon-i box-icon-i-2"><i class="fa fa-bolt"></i></div>
									<div class="box-icon-content">
										<h5>Awesome Shortcodes</h5>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Integer lorem quam adipiscing. <a href="#">Read More</a></p>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="box-icon">
									<div class="box-icon-i box-icon-i-2"><i class="fa fa-mobile-phone"></i></div>
									<div class="box-icon-content">
										<h5>Fully Responsive</h5>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Integer lorem quam adipiscing. <a href="#">Read More</a></p>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="box-icon">
									<div class="box-icon-i box-icon-i-2"><i class="fa fa-cog"></i></div>
									<div class="box-icon-content">
										<h5>Easy To Customize</h5>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Integer lorem quam adipiscing. <a href="#">Read More</a></p>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="box-icon">
									<div class="box-icon-i box-icon-i-2"><i class="fa fa-font"></i></div>
									<div class="box-icon-content">
										<h5>Google Fonts</h5>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Integer lorem quam adipiscing. <a href="#">Read More</a></p>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div><!-- End box-icon-slide -->
			</div><!-- End animation -->
		</div><!-- End row -->
	</div><!-- End container -->
</div><!-- End sections -->

<div class="sections section-2">
	<div class="container">
		<div class="animation" data-animate="bounceInDown">
			<div class="sections-title">
				<div class="sections-title-h3"><h3>Latest Awesome & Creative Works</h3></div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.</p>
			</div><!-- End sections-title -->
		</div><!-- End animation -->
		<div class="row">
			<div class="animation" data-animate="bounceInUp">
				<div class="portfolio-slide bxslider-slide">
					<ul>
						<li>
							<div class="col-md-3 portfolio-item">
								<div class="portfolio-one">
									<div class="portfolio-head">
										<div class="portfolio-img"><img alt="" src="http://placehold.it/345x340/171717/FFF/"></div>
										<div class="portfolio-hover">
											<a class="portfolio-link" href="single-portfolio.html"><i class="fa fa-link"></i></a>
											<a class="portfolio-zoom prettyPhoto" href="http://placehold.it/345x340/171717/FFF/"><i class="fa fa-search"></i></a>
										</div>
									</div><!-- End portfolio-head -->
									<div class="portfolio-content">
										<i class="fa fa-leaf"></i>
										<div class="portfolio-meta">
											<div class="portfolio-name"><h6><a href="single-portfolio.html">Project Name</a></h6></div>
											<div class="portfolio-cat"><a href="#">Category</a></div>
										</div><!-- End portfolio-meta -->
									</div><!-- End portfolio-content -->
								</div><!-- End portfolio-item -->
							</div>
							<div class="col-md-3 portfolio-item">
								<div class="portfolio-one">
									<div class="portfolio-head">
										<div class="portfolio-img"><img alt="" src="http://placehold.it/345x340/171717/FFF/"></div>
										<div class="portfolio-hover">
											<a class="portfolio-link" href="single-portfolio.html"><i class="fa fa-link"></i></a>
											<a class="portfolio-zoom prettyPhoto" href="http://placehold.it/345x340/171717/FFF/"><i class="fa fa-search"></i></a>
										</div>
									</div><!-- End portfolio-head -->
									<div class="portfolio-content">
										<i class="fa fa-leaf"></i>
										<div class="portfolio-meta">
											<div class="portfolio-name"><h6><a href="single-portfolio.html">Project Name</a></h6></div>
											<div class="portfolio-cat"><a href="#">Category</a></div>
										</div><!-- End portfolio-meta -->
									</div><!-- End portfolio-content -->
								</div><!-- End portfolio-item -->
							</div>
							<div class="col-md-3 portfolio-item">
								<div class="portfolio-one">
									<div class="portfolio-head">
										<div class="portfolio-img"><img alt="" src="http://placehold.it/345x340/171717/FFF/"></div>
										<div class="portfolio-hover">
											<a class="portfolio-link" href="single-portfolio.html"><i class="fa fa-link"></i></a>
											<a class="portfolio-zoom prettyPhoto" href="http://placehold.it/345x340/171717/FFF/"><i class="fa fa-search"></i></a>
										</div>
									</div><!-- End portfolio-head -->
									<div class="portfolio-content">
										<i class="fa fa-leaf"></i>
										<div class="portfolio-meta">
											<div class="portfolio-name"><h6><a href="single-portfolio.html">Project Name</a></h6></div>
											<div class="portfolio-cat"><a href="#">Category</a></div>
										</div><!-- End portfolio-meta -->
									</div><!-- End portfolio-content -->
								</div><!-- End portfolio-item -->
							</div>
							<div class="col-md-3 portfolio-item">
								<div class="portfolio-one">
									<div class="portfolio-head">
										<div class="portfolio-img"><img alt="" src="http://placehold.it/345x340/171717/FFF/"></div>
										<div class="portfolio-hover">
											<a class="portfolio-link" href="single-portfolio.html"><i class="fa fa-link"></i></a>
											<a class="portfolio-zoom prettyPhoto" href="http://placehold.it/345x340/171717/FFF/"><i class="fa fa-search"></i></a>
										</div>
									</div><!-- End portfolio-head -->
									<div class="portfolio-content">
										<i class="fa fa-leaf"></i>
										<div class="portfolio-meta">
											<div class="portfolio-name"><h6><a href="single-portfolio.html">Project Name</a></h6></div>
											<div class="portfolio-cat"><a href="#">Category</a></div>
										</div><!-- End portfolio-meta -->
									</div><!-- End portfolio-content -->
								</div><!-- End portfolio-item -->
							</div>
						</li>
						<li>
							<div class="col-md-3 portfolio-item">
								<div class="portfolio-one">
									<div class="portfolio-head">
										<div class="portfolio-img"><img alt="" src="http://placehold.it/345x340/171717/FFF/"></div>
										<div class="portfolio-hover">
											<a class="portfolio-link" href="single-portfolio.html"><i class="fa fa-link"></i></a>
											<a class="portfolio-zoom prettyPhoto" href="http://placehold.it/345x340/171717/FFF/"><i class="fa fa-search"></i></a>
										</div>
									</div><!-- End portfolio-head -->
									<div class="portfolio-content">
										<i class="fa fa-leaf"></i>
										<div class="portfolio-meta">
											<div class="portfolio-name"><h6><a href="single-portfolio.html">Project Name</a></h6></div>
											<div class="portfolio-cat"><a href="#">Category</a></div>
										</div><!-- End portfolio-meta -->
									</div><!-- End portfolio-content -->
								</div><!-- End portfolio-item -->
							</div>
							<div class="col-md-3 portfolio-item">
								<div class="portfolio-one">
									<div class="portfolio-head">
										<div class="portfolio-img"><img alt="" src="http://placehold.it/345x340/171717/FFF/"></div>
										<div class="portfolio-hover">
											<a class="portfolio-link" href="single-portfolio.html"><i class="fa fa-link"></i></a>
											<a class="portfolio-zoom prettyPhoto" href="http://placehold.it/345x340/171717/FFF/"><i class="fa fa-search"></i></a>
										</div>
									</div><!-- End portfolio-head -->
									<div class="portfolio-content">
										<i class="fa fa-leaf"></i>
										<div class="portfolio-meta">
											<div class="portfolio-name"><h6><a href="single-portfolio.html">Project Name</a></h6></div>
											<div class="portfolio-cat"><a href="#">Category</a></div>
										</div><!-- End portfolio-meta -->
									</div><!-- End portfolio-content -->
								</div><!-- End portfolio-item -->
							</div>
							<div class="col-md-3 portfolio-item">
								<div class="portfolio-one">
									<div class="portfolio-head">
										<div class="portfolio-img"><img alt="" src="http://placehold.it/345x340/171717/FFF/"></div>
										<div class="portfolio-hover">
											<a class="portfolio-link" href="single-portfolio.html"><i class="fa fa-link"></i></a>
											<a class="portfolio-zoom prettyPhoto" href="http://placehold.it/345x340/171717/FFF/"><i class="fa fa-search"></i></a>
										</div>
									</div><!-- End portfolio-head -->
									<div class="portfolio-content">
										<i class="fa fa-leaf"></i>
										<div class="portfolio-meta">
											<div class="portfolio-name"><h6><a href="single-portfolio.html">Project Name</a></h6></div>
											<div class="portfolio-cat"><a href="#">Category</a></div>
										</div><!-- End portfolio-meta -->
									</div><!-- End portfolio-content -->
								</div><!-- End portfolio-item -->
							</div>
							<div class="col-md-3 portfolio-item">
								<div class="portfolio-one">
									<div class="portfolio-head">
										<div class="portfolio-img"><img alt="" src="http://placehold.it/345x340/171717/FFF/"></div>
										<div class="portfolio-hover">
											<a class="portfolio-link" href="single-portfolio.html"><i class="fa fa-link"></i></a>
											<a class="portfolio-zoom prettyPhoto" href="http://placehold.it/345x340/171717/FFF/"><i class="fa fa-search"></i></a>
										</div>
									</div><!-- End portfolio-head -->
									<div class="portfolio-content">
										<i class="fa fa-leaf"></i>
										<div class="portfolio-meta">
											<div class="portfolio-name"><h6><a href="single-portfolio.html">Project Name</a></h6></div>
											<div class="portfolio-cat"><a href="#">Category</a></div>
										</div><!-- End portfolio-meta -->
									</div><!-- End portfolio-content -->
								</div><!-- End portfolio-item -->
							</div>
						</li>
					</ul>
				</div><!-- End portfolio-slide -->
			</div><!-- End animation -->
		</div><!-- End row -->
		<div class="animation" data-animate="bounceInRight">
			<div class="load-more-projects"><a class="button-1" href="portfolio-description.html">Load More Projects</a></div>
		</div><!-- End animation -->
	</div><!-- End container -->
</div><!-- End sections -->

<div class="sections sections-padding-b-50">
	<div class="container">
		<div class="animation" data-animate="fadeInUp">
			<div class="sections-title">
				<div class="sections-title-h3"><h3>All What You Need To Know About Us</h3></div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.</p>
			</div><!-- End sections-title -->
		</div><!-- End animation -->
		<div class="row">
			<div class="col-md-6">
				<div class="animation" data-animate="fadeInRight">
					<div class="accordion">
						<div class="section-content">
						    <h4 class="accordion-title active"><a href="#">What Is Ori ?<i class="fa fa-plus"></i></a></h4>
						    <div class="accordion-inner active">
						        Duis dapibus aliquam mi, eget euismod scelerisque ut. Vivamus at elit quis urna adipiscing , Curabitur vitae velit in neque dictum blandit. Duis dapibus aliquam mi, eget euismod sceler ut.Duis dapibus aliquam mi, eget euismod scelerisque at elit quis urna adipiscing , Curabitur vitae velit in neque dictum blandit. Duis dapibus aliquam mi, eget euismod sceler ut.
						    </div>
					    </div>
					    <div class="section-content">
						    <h4 class="accordion-title"><a href="#">Why Should I Buy Ori ?<i class="fa fa-minus"></i></a></h4>
						    <div class="accordion-inner">
						        Duis dapibus aliquam mi, eget euismod scelerisque ut. Vivamus at elit quis urna adipiscing , Curabitur vitae velit in neque dictum blandit. Duis dapibus aliquam mi, eget euismod sceler ut.Duis dapibus aliquam mi, eget euismod scelerisque at elit quis urna adipiscing , Curabitur vitae velit in neque dictum blandit. Duis dapibus aliquam mi, eget euismod sceler ut.
						    </div>
					    </div>
					    <div class="section-content">
						    <h4 class="accordion-title"><a href="#">What Is Ori Features ?<i class="fa fa-minus"></i></a></h4>
						    <div class="accordion-inner">
						        Duis dapibus aliquam mi, eget euismod scelerisque ut. Vivamus at elit quis urna adipiscing , Curabitur vitae velit in neque dictum blandit. Duis dapibus aliquam mi, eget euismod sceler ut.Duis dapibus aliquam mi, eget euismod scelerisque at elit quis urna adipiscing , Curabitur vitae velit in neque dictum blandit. Duis dapibus aliquam mi, eget euismod sceler ut.
						    </div>
					    </div>
					</div><!-- End accordion -->
				</div><!-- End animation -->
			</div>
			<div class="col-md-6">
				<div class="animation" data-animate="bounceInLeft">
					<div class="progressbar-warp">
					    <div class="progressbar">
						    <span class="progressbar-title">Design<span>70%</span></span>
						    <div class="progressbar-all">
						        <div class="progressbar-percent" style="background-color: #e03e25;" data-percent="70"></div>
					        </div>
					    </div>
					    <div class="progressbar">
						    <span class="progressbar-title">Branding<span>50%</span></span>
						    <div class="progressbar-all">
						        <div class="progressbar-percent" style="background-color: #e03e25;" data-percent="50"></div>
					        </div>
					    </div>
					    <div class="progressbar">
						    <span class="progressbar-title">Development<span>85%</span></span>
						    <div class="progressbar-all">
					        	<div class="progressbar-percent" style="background-color: #e03e25;" data-percent="85"></div>
					        </div>
					    </div>
					    <div class="progressbar">
						    <span class="progressbar-title">Wordpress<span>100%</span></span>
						    <div class="progressbar-all">
						        <div class="progressbar-percent" style="background-color: #e03e25;" data-percent="100"></div>
					        </div>
					    </div>
					    <div class="progressbar">
						    <span class="progressbar-title">Game UI Design<span>70%</span></span>
						    <div class="progressbar-all">
						        <div class="progressbar-percent" style="background-color: #e03e25;" data-percent="70"></div>
					        </div>
					    </div>
					</div><!-- End progressbar-warp -->
				</div><!-- End animation -->
			</div>
		</div><!-- End row -->
	</div><!-- End container -->
</div><!-- End sections -->

<div class="sections section-3">
	<div class="container">
		<div class="animation" data-animate="bounceInDown">
			<div class="sections-title">
				<div class="sections-title-h3"><h3>Ori Great & Awesome Features</h3></div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.</p>
			</div><!-- End sections-title -->
		</div><!-- End animation -->
		<div class="row">
			<div class="col-md-3">
				<div class="animation" data-animate="bounceInUp">
					<div class="box-icon">
						<div class="box-icon-i box-icon-i-2 box-icon-i-4"><i class="fa fa-bolt"></i></div>
						<div class="box-icon-content">
							<h5>Awesome Shortcodes</h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Integer lorem quam.</p>
						</div>
					</div>
				</div><!-- End animation -->
				<div class="gap"></div>
				<div class="animation" data-animate="bounceInRight">
					<div class="box-icon">
						<div class="box-icon-i box-icon-i-2 box-icon-i-4"><i class="fa fa-mobile-phone"></i></div>
						<div class="box-icon-content">
							<h5>Fully Responsive</h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Integer lorem quam.</p>
						</div>
					</div>
				</div><!-- End animation -->
			</div>
			<div class="col-md-6">
				<div class="gap"></div>
				<div class="gap"></div>
				<div class="animation" data-animate="fadeInLeft">
					<div class="t_center"><img alt="" src="images/macbookpro.png"></div>
				</div><!-- End animation -->
				<div class="gap"></div>
			</div>
			<div class="col-md-3">
				<div class="animation" data-animate="rotateInDownLeft">
					<div class="box-icon">
						<div class="box-icon-i box-icon-i-2 box-icon-i-4"><i class="fa fa-cog"></i></div>
						<div class="box-icon-content">
							<h5>Easy To Customize</h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Integer lorem quam.</p>
						</div>
					</div>
				</div><!-- End animation -->
				<div class="gap"></div>
				<div class="animation" data-animate="rotateInDownRight">
					<div class="box-icon">
						<div class="box-icon-i box-icon-i-2 box-icon-i-4"><i class="fa fa-font"></i></div>
						<div class="box-icon-content">
							<h5>Google Fonts</h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Integer lorem quam.</p>
						</div>
					</div>
				</div><!-- End animation -->
			</div>
		</div><!-- End row -->
	</div><!-- End container -->
</div><!-- End sections -->

<div class="sections sections-padding-b-50">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="animation" data-animate="bounceInDown">
					<div class="head-title"><h6>Latest News</h6></div>
					<div class="row">
						<div class="bxslider-slide bxslider-slide-title blog-silde">
							<ul>
								<li>
									<div class="col-md-6 blog-item">
										<div class="blog-one">
											<div class="blog-img">
												<img alt="" src="http://placehold.it/370x170/171717/FFF/">
												<div class="blog-date"><span>3</span><span>July, 2014</span></div>
											</div><!-- End blog-img -->
											<div class="blog-content">
												<h6><a href="single-blog.html">Blog Tiltle Shall Be Here !</a></h6>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, turpis. <a href="single-blog.html">Read More</a></p>
											</div><!-- End blog-content -->
											<div class="blog-meta">
												<div class="blog-author"><i class="fa fa-user"></i>Author : <a href="#">Begha</a></div>
												<div class="blog-comments"><i class="fa fa-comments"></i>Comments : <a href="#">22</a></div>
											</div><!-- End blog-meta -->
											<div class="clearfix"></div>
										</div><!-- End blog-item -->
									</div>
									<div class="col-md-6 blog-item">
										<div class="blog-one">
											<div class="blog-img">
												<img alt="" src="http://placehold.it/370x170/171717/FFF/">
												<div class="blog-date"><span>2</span><span>July, 2014</span></div>
											</div><!-- End blog-img -->
											<div class="blog-content">
												<h6><a href="single-blog.html">Blog Tiltle Shall Be Here !</a></h6>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, turpis. <a href="single-blog.html">Read More</a></p>
											</div><!-- End blog-content -->
											<div class="blog-meta">
												<div class="blog-author"><i class="fa fa-user"></i>Author : <a href="#">vbegy</a></div>
												<div class="blog-comments"><i class="fa fa-comments"></i>Comments : <a href="#">22</a></div>
											</div><!-- End blog-meta -->
											<div class="clearfix"></div>
										</div><!-- End blog-item -->
									</div>
								</li>
								<li>
									<div class="col-md-6 blog-item">
										<div class="blog-one">
											<div class="blog-img">
												<img alt="" src="http://placehold.it/370x170/171717/FFF/">
												<div class="blog-date"><span>3</span><span>July, 2014</span></div>
											</div><!-- End blog-img -->
											<div class="blog-content">
												<h6><a href="single-blog.html">Blog Tiltle Shall Be Here !</a></h6>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, turpis. <a href="single-blog.html">Read More</a></p>
											</div><!-- End blog-content -->
											<div class="blog-meta">
												<div class="blog-author"><i class="fa fa-user"></i>Author : <a href="#">Begha</a></div>
												<div class="blog-comments"><i class="fa fa-comments"></i>Comments : <a href="#">22</a></div>
											</div><!-- End blog-meta -->
											<div class="clearfix"></div>
										</div><!-- End blog-item -->
									</div>
									<div class="col-md-6 blog-item">
										<div class="blog-one">
											<div class="blog-img">
												<img alt="" src="http://placehold.it/370x170/171717/FFF/">
												<div class="blog-date"><span>2</span><span>July, 2014</span></div>
											</div><!-- End blog-img -->
											<div class="blog-content">
												<h6><a href="single-blog.html">Blog Tiltle Shall Be Here !</a></h6>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, turpis. <a href="single-blog.html">Read More</a></p>
											</div><!-- End blog-content -->
											<div class="blog-meta">
												<div class="blog-author"><i class="fa fa-user"></i>Author : <a href="#">vbegy</a></div>
												<div class="blog-comments"><i class="fa fa-comments"></i>Comments : <a href="#">22</a></div>
											</div><!-- End blog-meta -->
											<div class="clearfix"></div>
										</div><!-- End blog-item -->
									</div>
								</li>
							</ul>
						</div><!-- End blog-silde -->
					</div><!-- End row -->
				</div><!-- End animation -->
			</div>
			<div class="col-md-4">
				<div class="animation" data-animate="bounceInUp">
					<div class="head-title"><h6>Testimonials</h6></div>
					<div class="row">
						<div class="bxslider-slide bxslider-slide-title testimonials-slide">
							<ul>
								<li>
									<div class="col-md-12">
										<div class="testimonial-item">
											<div class="testimonial-content">
												<div><i class="fa fa-quote-left"></i></div>
												<p>Duis dapibus aliquam mi, eget euismod scelerisque ut. Vivamus at elit quis urna adipiscing , Curabitur vitae velit in neque dictum blandit. Duis dapibus aliquam mi, eget euismod sceler ut.Duis dapibus aliquam mi, eget euismod scelerisque at elit quis urna adipiscing , Curabitur vitae velit in neque dictum bldit.</p>
												<div><i class="fa fa-quote-right"></i></div>
											</div><!-- End testimonial-content -->
											<div class="testimonial-meta">
												<div class="testimonial-img"><img alt="" src="http://placehold.it/60x60/FFF/171717/"></div>
												<div class="testimonial-name"><span>Begha</span><span>7oroof.com Network</span></div>
											</div><!-- End testimonial-meta -->
											<div class="clearfix"></div>
										</div><!-- End testimonial-item -->
									</div>
								</li>
								<li>
									<div class="col-md-12">
										<div class="testimonial-item">
											<div class="testimonial-content">
												<div><i class="fa fa-quote-left"></i></div>
												<p>Duis dapibus aliquam mi, eget euismod scelerisque ut. Vivamus at elit quis urna adipiscing , Curabitur vitae velit in neque dictum blandit. Duis dapibus aliquam mi, eget euismod sceler ut.Duis dapibus aliquam mi, eget euismod scelerisque at elit quis urna adipiscing , Curabitur vitae velit in neque dictum bldit.</p>
												<div><i class="fa fa-quote-right"></i></div>
											</div><!-- End testimonial-content -->
											<div class="testimonial-meta">
												<div class="testimonial-img"><img alt="" src="http://placehold.it/60x60/FFF/171717/"></div>
												<div class="testimonial-name"><span>vbegy</span><span>2code.info Network</span></div>
											</div><!-- End testimonial-meta -->
											<div class="clearfix"></div>
										</div><!-- End testimonial-item -->
									</div>
								</li>
							</ul>
						</div><!-- End testimonials-slide -->
					</div><!-- End row -->
				</div><!-- End animation -->
			</div>
		</div><!-- End row -->
	</div><!-- End container -->
</div><!-- End sections -->

<div class="sections sections-padding-t-0 sections-padding-b-50">
	<div class="container">
		<div class="animation" data-animate="bounceInRight">
			<div class="head-title"><h6>Our Clients</h6></div>
			<div class="row">
				<div class="bxslider-slide bxslider-slide-title clients-slide">
					<ul>
						<li>
							<div class="col-md-2 client-item">
								<div class="client"><a href="#"><img alt="" src="http://placehold.it/170x70/FFF/171717/"></a></div>
							</div>
							<div class="col-md-2 client-item">
								<div class="client"><a href="#"><img alt="" src="http://placehold.it/170x70/FFF/171717/"></a></div>
							</div>
							<div class="col-md-2 client-item">
								<div class="client"><a href="#"><img alt="" src="http://placehold.it/170x70/FFF/171717/"></a></div>
							</div>
							<div class="col-md-2 client-item">
								<div class="client"><a href="#"><img alt="" src="http://placehold.it/170x70/FFF/171717/"></a></div>
							</div>
							<div class="col-md-2 client-item">
								<div class="client"><a href="#"><img alt="" src="http://placehold.it/170x70/FFF/171717/"></a></div>
							</div>
							<div class="col-md-2 client-item">
								<div class="client"><a href="#"><img alt="" src="http://placehold.it/170x70/FFF/171717/"></a></div>
							</div>
						</li>
						<li>
							<div class="col-md-2 client-item">
								<div class="client"><a href="#"><img alt="" src="http://placehold.it/170x70/FFF/171717/"></a></div>
							</div>
							<div class="col-md-2 client-item">
								<div class="client"><a href="#"><img alt="" src="http://placehold.it/170x70/FFF/171717/"></a></div>
							</div>
							<div class="col-md-2 client-item">
								<div class="client"><a href="#"><img alt="" src="http://placehold.it/170x70/FFF/171717/"></a></div>
							</div>
							<div class="col-md-2 client-item">
								<div class="client"><a href="#"><img alt="" src="http://placehold.it/170x70/FFF/171717/"></a></div>
							</div>
							<div class="col-md-2 client-item">
								<div class="client"><a href="#"><img alt="" src="http://placehold.it/170x70/FFF/171717/"></a></div>
							</div>
							<div class="col-md-2 client-item">
								<div class="client"><a href="#"><img alt="" src="http://placehold.it/170x70/FFF/171717/"></a></div>
							</div>
						</li>
					</ul>
				</div><!-- End clients-slide -->
			</div><!-- End row -->
		</div><!-- End animation -->
	</div><!-- End container -->
</div><!-- End sections -->