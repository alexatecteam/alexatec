<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="{{ app()->getLocale() }}"> 
<!--<![endif]-->
<head>

	<!-- Basic Page Needs -->
	<meta charset="utf-8">
	<title>Ori – Multi-purpose Business HTML Template</title>
	<meta name="description" content="Ori – Multi-purpose Business HTML Template">
	<meta name="author" content="2code.info">
	
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Main Style -->
	<link rel="stylesheet" href="/css/style.css">
	
	<!-- Skins -->
	<link rel="stylesheet" href="/css/skins/skins.css">
	
	<!-- Responsive Style -->
	<link rel="stylesheet" href="/css/responsive.css">
	
	<!-- Favicons -->
	<link rel="shortcut icon" href="/images/favicon.png">
  
</head>
<body>

<div class="loader"><div class="loader_html"></div></div>

<div id="wrap" class="grid_1200">